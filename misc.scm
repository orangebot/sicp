(define (perf thunk)
	(with-timings
		thunk
		(lambda (run-time gc-time real-time)
			(write (internal-time/ticks->seconds run-time))
			(write-char #\space)
			(write (internal-time/ticks->seconds gc-time))
			(write-char #\space)
			(write (internal-time/ticks->seconds real-time))
			(newline))))

(define (cache func)
	(let (	(c '()) )
		(lambda args
			(let (	(cached (assoc args c)) )
				(if cached
					(cadr cached)
					(let (	(result (apply func args)) )
						(set! c (cons (list args result) c))
						result))))))
