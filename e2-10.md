## Question

Ben Bitdiddle, an expert systems programmer, looks over Alyssa's shoulder
and comments that it is not clear what it means to divide by an interval
that spans zero. Modify Alyssa's code to check for this condition and
to signal an error if it occurs.


## Answer

Again, no definition for 'spans zero', so that could mean that the
interval includes 0 (something like (-1 . 1)), one of the bounds is 0
(something like (0 . 1)), or the width is 0 (something like (1 . 1)).
Well, time to test it.

1st case:

```
> (div-interval (make-interval 1 2) (make-interval -1 1))
(-2.0 . 2.0)
```

Seems fine.

2nd case:

```
> (div-interval (make-interval 1 2) (make-interval 0 1))
(1.0 . +inf.0)
```

That definitely causes a problem. It seems that lisp can kind of handle
it though. Should put a check in anyway.

3rd case:

```
> (div-interval (make-interval 1 2) (make-interval 1 1))
(1.0 . 2.0)
```

I don't notice a problem with this one.

Assuming it's the 2nd case, I would just throw and error or something
when one of the numbers in the interval is a 0.
