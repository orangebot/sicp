## Question

A function f is defined by the rule that f(n) = n if n<3 and f(n) = f(n - 1) +
2f(n - 2) + 3f(n - 3) if n≥ 3. Write a procedure that computes f by means of a
recursive process. Write a procedure that computes f by means of an iterative
process.

f(n) = if n<3, n
f(n) = if n≥3, f(n-1) + 2*f(n-2) + 3*f(n-3)



## Answer

```
1 ]=> (map f '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))

;Value 11: (1 2 4 11 25 59 142 335 796 1892 4489 10661 25315 60104 142717)
```

Recursive

```
(define (f n)
	(if (< n 3)
		n
		(+
			(f (- n 1))
			(* 2 (f (- n 2)))
			(* 3 (f (- n 3))))))
```

Iterative

```
(define (f n)
	(if (< n 3)
		n
		(f-iter 2 1 0 n)))

(define (f-iter a b c n)
	(if (< n 3)
		a
		(f-iter
			(+ a (* 2 b) (* 3 c))
			a
			b
			(- n 1))))
```
