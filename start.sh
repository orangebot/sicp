#!/bin/sh

# Set up an environment to work on this easily.

cd `dirname $0`
acmewrapper -l $HOME/lib/sicp.acme &
plumb `head -n 1 bookmark.txt`

